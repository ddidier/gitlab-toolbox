#!/usr/bin/env bash

# shellcheck source-path=SCRIPTDIR/.. disable=SC1090
source "${NDD_TEST4B_LIB_DIR}/ansi/ansi"
# shellcheck source-path=SCRIPTDIR/.. disable=SC1090
source "${NDD_TEST4B_LIB_DIR}/ndd-log4b/ndd-log4b.sh"
# shellcheck source-path=SCRIPTDIR/.. disable=SC1090
source "${NDD_TEST4B_LIB_DIR}/ndd-utils4b/ndd-utils4b.sh"

ndd::logger::set_stdout_level "${NDD_TEST4B_MOCK_STDOUT_LEVEL}"

log_mock_command() {
  local command="${1}"
  echo "${command}" >>"${NDD_TEST4B_CAPTURED_MOCK_FILE}"
}

unexpected_mock_command() {
  local command="${1}"
  local arguments="${2}"

  log fatal "Unexpected arguments for mocked command:"
  log fatal "  Command: %s" "${command}"
  log fatal "  Arguments: %s" "${arguments}"
  exit 1
}
