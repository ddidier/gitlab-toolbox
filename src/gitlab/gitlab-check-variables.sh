#!/usr/bin/env bash

NDD_TOOLBOX_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck source-path=SCRIPTDIR/../.. disable=SC1090
source "${NDD_TOOLBOX_DIR}/lib/ansi/ansi"
# shellcheck source-path=SCRIPTDIR/../.. disable=SC1090
source "${NDD_TOOLBOX_DIR}/lib/ndd-log4b/ndd-log4b.sh"
# shellcheck source-path=SCRIPTDIR/../.. disable=SC1090
source "${NDD_TOOLBOX_DIR}/lib/ndd-utils4b/ndd-utils4b.sh"

# disable before shflags
ndd::base::catch_more_errors_off

# shellcheck source-path=SCRIPTDIR/../.. disable=SC1090
source "${NDD_TOOLBOX_DIR}/lib/shflags/shflags"

DEFINE_boolean "debug" false "Enable debug mode" "d"
DEFINE_string  "mandatory-private-variable-names" "" "The names of the mandatory private environment variables" ""
DEFINE_string  "mandatory-public-variable-names"  "" "The names of the mandatory public environment variables"  ""
DEFINE_string  "optional-private-variable-names"  "" "The names of the optional private environment variables"  ""
DEFINE_string  "optional-public-variable-names"   "" "The names of the optional public environment variables"   ""
DEFINE_string  "forbidden-variable-names"         "" "The names of the forbidden environment variables"         ""

read -r -d '' FLAGS_HELP <<EOF
Check that the given environment variables have a value.
Variables may be either public or private, in which case their value will be masked.
Variables may be either optional or mandatory, in which case their value must be set.
Some variables must not be set depending on circumstances, for example a sign key in a not protected pipeline.
The variables names arguments are separated by spaces, e.g. "VARIABLE1 VARIABLE2 VARIABLE3".
EOF

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# enable after shflags
ndd::base::catch_more_errors_on

function main() {

  if [[ "${FLAGS_debug}" -eq "${FLAGS_TRUE}" ]]; then
    ndd::logger::set_stdout_level "DEBUG"
  else
    ndd::logger::set_stdout_level "INFO"
  fi

  # shellcheck disable=SC2154
  IFS=" " read -r -a mandatory_private_variable_names <<< "${FLAGS_mandatory_private_variable_names}"
  # shellcheck disable=SC2154
  IFS=" " read -r -a mandatory_public_variable_names <<< "${FLAGS_mandatory_public_variable_names}"
  # shellcheck disable=SC2154
  IFS=" " read -r -a optional_private_variable_names <<< "${FLAGS_optional_private_variable_names}"
  # shellcheck disable=SC2154
  IFS=" " read -r -a optional_public_variable_names <<< "${FLAGS_optional_public_variable_names}"
  # shellcheck disable=SC2154
  IFS=" " read -r -a forbidden_variable_names <<< "${FLAGS_forbidden_variable_names}"

  log debug "Mandatory private environment variables = ${mandatory_private_variable_names[*]}"
  log debug "Mandatory public environment variables  = ${mandatory_public_variable_names[*]}"
  log debug "Optional private environment variables  = ${optional_private_variable_names[*]}"
  log debug "Optional public environment variables   = ${optional_public_variable_names[*]}"
  log debug "Forbidden environment variables         = ${forbidden_variable_names[*]}"

  log info "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  log info "┃ CI environment"
  log info "┣━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"

  local missing_environment_variable_names=()
  local set_forbidden_variable_names=()

  log info "┃ Mandatory private environment variables:"

  for mandatory_private_variable_name in "${mandatory_private_variable_names[@]}"; do
    if [[ -z ${!mandatory_private_variable_name+x} ]]; then
      missing_environment_variable_names+=("${mandatory_private_variable_name}")
      log error "┃ - %s = %s" "${mandatory_private_variable_name}" "MISSING!"
    else
      log info "┃ - %s = %s" "${mandatory_private_variable_name}" "[MASKED]"
    fi
  done

  log info "┃ Mandatory public environment variables:"

  for mandatory_public_variable_name in "${mandatory_public_variable_names[@]}"; do
    if [[ -z ${!mandatory_public_variable_name+x} ]]; then
      missing_environment_variable_names+=("${mandatory_public_variable_name}")
      log error "┃ - %s = %s" "${mandatory_public_variable_name}" "MISSING!"
    else
      log info "┃ - %s = %s" "${mandatory_public_variable_name}" "${!mandatory_public_variable_name}"
    fi
  done

  log info "┃ Optional private environment variables:"

  for optional_private_variable_name in "${optional_private_variable_names[@]}"; do
    if [[ -z ${!optional_private_variable_name+x} ]]; then
      log info "┃ - %s = %s" "${optional_private_variable_name}" "MISSING!"
    else
      log info "┃ - %s = %s" "${optional_private_variable_name}" "[MASKED]"
    fi
  done

  log info "┃ Optional public environment variables:"

  for optional_public_variable_name in "${optional_public_variable_names[@]}"; do
    if [[ -z ${!optional_public_variable_name+x} ]]; then
      log info "┃ - %s = %s" "${optional_public_variable_name}" "MISSING!"
    else
      log info "┃ - %s = %s" "${optional_public_variable_name}" "${!optional_public_variable_name}"
    fi
  done

  log info "┃ Forbidden environment variables:"

  for forbidden_variable_name in "${forbidden_variable_names[@]}"; do
    if [[ -z ${!forbidden_variable_name+x} ]]; then
      log info "┃ - %s = %s" "${forbidden_variable_name}" "NOT SET"
    else
      set_forbidden_variable_names+=("${forbidden_variable_name}")
      log info "┃ - %s = %s" "${forbidden_variable_name}" "SET!"
    fi
  done

  log info "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"

  local failure="false"

  if [ ${#missing_environment_variable_names[@]} -eq 0 ]; then
    log info "$(ndd::symbols::check_mark "All mandatory environment variables are set! Proceeding!")"
  else
    log fatal "$(ndd::symbols::fatal "Some mandatory environment variables are missing! Exiting!")"
    failure="true"
  fi

  if [ ${#set_forbidden_variable_names[@]} -eq 0 ]; then
    log info "$(ndd::symbols::check_mark "No forbidden environment variables is set! Proceeding!")"
  else
    log fatal "$(ndd::symbols::fatal "Some forbidden environment variables are set! Exiting!")"
    failure="true"
  fi

  if [[ "${failure}" == "true" ]]; then
    exit 1
  fi
}

main "${@}"
