#!/usr/bin/env bash

# ------------------------------------------------------------------------------
# Release the Maven project.
# Required environment variables:
# - CI_COMMIT_TAG: The commit tag name, available only in pipelines for tags (from the GitLab CI pipeline).
# ------------------------------------------------------------------------------

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

echo "Releasing Maven project"

release-cli create \
  --name "Version ${CI_COMMIT_TAG}" \
  --tag-name "${CI_COMMIT_TAG}"
