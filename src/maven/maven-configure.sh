#!/usr/bin/env bash

NDD_GITLAB_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${NDD_GITLAB_DIR}/lib/ansi/ansi"
# shellcheck disable=SC1090
source "${NDD_GITLAB_DIR}/lib/ndd-log4b/ndd-log4b.sh"
# shellcheck disable=SC1090
source "${NDD_GITLAB_DIR}/lib/ndd-utils4b/ndd-utils4b.sh"

# enable after shflags
ndd::base::catch_more_errors_on

function main() {

  mkdir -p "${CI_PROJECT_DIR}/.m2"

  # define a custom Maven repository directory
  export MAVEN_REPOSITORY_PATH="${CI_PROJECT_DIR}/.m2/repository"

  # define a custom Maven settings file
  export MAVEN_SETTINGS_PATH="${CI_PROJECT_DIR}/.m2/settings.xml"

  # copy the custom Maven settings file from the toolbox
  cp "${NDD_GITLAB_DIR}/src/maven/settings-ci.xml" "${MAVEN_SETTINGS_PATH}"

  local maven_cli_options=(
    "--batch-mode"
    "--errors"
    "--fail-at-end"
    "--settings=${MAVEN_SETTINGS_PATH}"
  # "--show-version"
    "-Djava.awt.headless=true"
    "-Dmaven.repo.local=${MAVEN_REPOSITORY_PATH}"
    "-Dorg.slf4j.simpleLogger.showDateTime=true"
  )

  local maven_plugins_cli_options=(
    "-DinstallAtEnd=true"
    "-DdeployAtEnd=true"
    "-Dsonar.host.url=${SONAR_URL}"
    "-Dsonar.login=${SONAR_TOKEN}"
  )

  MAVEN_CLI_OPTIONS="${maven_cli_options[*]} ${maven_plugins_cli_options[*]}"

  export MAVEN_CLI_OPTIONS

  echo
  echo "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  echo "┃ NDD CI Maven environment"
  echo "┣━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  echo "┃ MAVEN_REPOSITORY_PATH = ${MAVEN_REPOSITORY_PATH}"
  echo "┃ MAVEN_SETTINGS_PATH   = ${MAVEN_SETTINGS_PATH}"
  echo "┃ MAVEN_CLI_OPTIONS     = ${MAVEN_CLI_OPTIONS}"
  echo "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  echo

  # shellcheck disable=SC2086
  mvn ${MAVEN_CLI_OPTIONS} --show-version help:effective-settings
  echo
}

main "${@}"
