#!/usr/bin/env bash

NDD_GITLAB_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${NDD_GITLAB_DIR}/lib/ansi/ansi"
# shellcheck disable=SC1090
source "${NDD_GITLAB_DIR}/lib/ndd-log4b/ndd-log4b.sh"
# shellcheck disable=SC1090
source "${NDD_GITLAB_DIR}/lib/ndd-utils4b/ndd-utils4b.sh"

# enable after shflags
ndd::base::catch_more_errors_on

function main() {

  # shellcheck disable=SC1090
  source "${NDD_GITLAB_DIR}/src/maven/maven-configure.sh"

  # Maven profile 'ndd.deploy' is defined in the 'ndd-maven-parent' POM.
  # Tests are disabled because they have been played in the 'maven-build.sh' during the 'build' job.
  # shellcheck disable=SC2086
  mvn ${MAVEN_CLI_OPTIONS} -Pndd.deploy -Dmaven.test.skip=true -Ddependency-check.skip=true clean deploy
}

main "${@}"
