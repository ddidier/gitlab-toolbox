#!/usr/bin/env bash

# ------------------------------------------------------------------------------
# Run SonarQube analysis.
# Required environment variables:
# - MAVEN_CLI_OPTIONS: The Maven options.
# - CI_COMMIT_BRANCH: The commit branch name.
# ------------------------------------------------------------------------------

NDD_GITLAB_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${NDD_GITLAB_DIR}/lib/ansi/ansi"
# shellcheck disable=SC1090
source "${NDD_GITLAB_DIR}/lib/ndd-log4b/ndd-log4b.sh"
# shellcheck disable=SC1090
source "${NDD_GITLAB_DIR}/lib/ndd-utils4b/ndd-utils4b.sh"

# enable after shflags
ndd::base::catch_more_errors_on

function main() {

  # shellcheck disable=SC1090
  source "${NDD_GITLAB_DIR}/src/maven/maven-configure.sh"

  local group_id
  local artifact_id

  # shellcheck disable=SC2086
  group_id="$(mvn ${MAVEN_CLI_OPTIONS} -q -DforceStdout -Dexpression="project.groupId" help:evaluate)"
  # shellcheck disable=SC2086
  artifact_id="$(mvn ${MAVEN_CLI_OPTIONS} -q -DforceStdout -Dexpression="project.artifactId" help:evaluate)"

  local sonar_project_key="ndd.${group_id}.${artifact_id}"

  echo "Launching Sonarqube analysis with 'sonar.projectKey' set to '${sonar_project_key}'"

  # shellcheck disable=SC2086
  mvn ${MAVEN_CLI_OPTIONS} -Dsonar.projectKey="${sonar_project_key}" -Dsonar.branch.name="${CI_COMMIT_BRANCH}" sonar:sonar
}

main "${@}"
