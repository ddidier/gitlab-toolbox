#!/usr/bin/env bash

NDD_GITLAB_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${NDD_GITLAB_DIR}/lib/ansi/ansi"
# shellcheck disable=SC1090
source "${NDD_GITLAB_DIR}/lib/ndd-log4b/ndd-log4b.sh"
# shellcheck disable=SC1090
source "${NDD_GITLAB_DIR}/lib/ndd-utils4b/ndd-utils4b.sh"

# disable before shflags
ndd::base::catch_more_errors_off

# shellcheck disable=SC1090
source "${NDD_GITLAB_DIR}/lib/shflags/shflags"

DEFINE_boolean "check-dependencies" false "Enable 'dependency-lock-maven-plugin' check"

# read -r -d '' FLAGS_HELP <<EOF
# Build the Maven project.
# EOF

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# enable after shflags
ndd::base::catch_more_errors_on

function main() {

  # shellcheck disable=SC1090
  source "${NDD_GITLAB_DIR}/src/maven/maven-configure.sh"

  # shellcheck disable=SC2154
  if [[ "${FLAGS_check_dependencies}" -eq "${FLAGS_TRUE}" ]]; then
    # shellcheck disable=SC2086
    mvn ${MAVEN_CLI_OPTIONS} se.vandmo:dependency-lock-maven-plugin:check
  fi

  # shellcheck disable=SC2086
  mvn ${MAVEN_CLI_OPTIONS} -DnddBuild=fast verify

  # convert JaCoCo report to a suitable output for GitLab
  local jacoco_csv_file="${CI_PROJECT_DIR}/target/site/jacoco/jacoco.csv"

  if [[ -f "${jacoco_csv_file}" ]]; then
      awk -F "," '
      {
        instructions += $4 + $5; covered += $5
      }
      END {
        print covered, "/", instructions, "instructions covered";
        printf "%.2f%% covered\n", covered / instructions * 100
      }' "${jacoco_csv_file}"
  fi
}

main "${@}"
