#!/usr/bin/env bash

# ------------------------------------------------------------------------------
# Build the HTML documentation.
# Required environment variables: none.
#
# One cannot use the helper script 'bin/build-html.sh' because of:
# - https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1482
# - https://gitlab.com/gitlab-org/gitlab-foss/-/issues/41227
# ------------------------------------------------------------------------------

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

echo "Building HTML documentation"

make --makefile=Makefile-sphinx html
