#!/usr/bin/env bash

# ------------------------------------------------------------------------------
# Check that the version of the Docker image inside of which this script is
# running is the same than the one declared in the 'bin/variables.sh' file.
# The file 'bin/variables.sh' must be sourced before calling this script.
# Required environment variables:
# - DDIDIER_SPHINX_IMAGE_VERSION: The Docker image version defined in 'bin/variables.sh'.
# - GIT_COMMIT_TAG: The Docker image version defined inside the Docker image running the job.
#
# One cannot use the helper script 'bin/build-html.sh' because of:
# - https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1482
# - https://gitlab.com/gitlab-org/gitlab-foss/-/issues/41227
# ------------------------------------------------------------------------------

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

echo "Checking Docker image version"

if [[ -z "${DDIDIER_SPHINX_IMAGE_VERSION+x}" ]]; then
  echo "The environment variable 'DDIDIER_SPHINX_IMAGE_VERSION' is not set."
  echo "Please source './bin/variables.sh' before calling this script. Aborting!"
  exit 1
fi

if [[ -z "${GIT_COMMIT_TAG+x}" ]]; then
  echo "The environment variable 'GIT_COMMIT_TAG' is not set."
  echo "Please use a 'ddidier/sphinx-doc' Docker image for this job. Aborting!"
  exit 1
fi

if [[ "${DDIDIER_SPHINX_IMAGE_VERSION}" != "${GIT_COMMIT_TAG}" ]]; then
  echo "Actual Docker image version   = ${GIT_COMMIT_TAG}"
  echo "Expected Docker image version = ${DDIDIER_SPHINX_IMAGE_VERSION}"
  echo "Docker image versions are different. Aborting!"
  exit 1
fi

echo "Docker image version is valid"
