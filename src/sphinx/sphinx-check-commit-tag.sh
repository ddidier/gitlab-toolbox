#!/usr/bin/env bash

# ------------------------------------------------------------------------------
# Check that if a Git commit tag exists, it conforms to "X.Y.Z".
# See https://docs.gitlab.com/ee/user/packages/generic_packages/#publish-a-package-file.
# Required environment variables:
# - CI_COMMIT_TAG: The commit tag name, available only in pipelines for tags (from the GitLab CI pipeline).
# ------------------------------------------------------------------------------

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

SEMVER_REGEX='^(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)$'

echo "Checking Git commit tag format"

if [[ -n ${CI_COMMIT_TAG+x} ]]; then

  echo "Git commit tag is defined as '${CI_COMMIT_TAG}'"

  if [[ ! ${CI_COMMIT_TAG} =~ ${SEMVER_REGEX} ]]; then
    echo "Git commit tag does not conform to 'X.Y.Z'"
    echo "See https://docs.gitlab.com/ee/user/packages/generic_packages/#publish-a-package-file"
    echo "Aborting!"
    exit 1
  fi

  echo "Git commit tag is valid"

else

  echo "Git commit tag is undefined"

fi
