#!/usr/bin/env bash

# ------------------------------------------------------------------------------
# Release the HTML documentation.
# Required environment variables:
# - CI_COMMIT_TAG: The commit tag name, available only in pipelines for tags (from the GitLab CI pipeline).
# - CI_JOB_TOKEN: A token to authenticate with certain API endpoints (from the GitLab CI pipeline).
# - PACKAGE_SHA_FILE_NAME: The name of the package archive using the commit SHA (from 'variables' of 'sphinx-gitlab.yaml').
# - PACKAGE_TAG_FILE_NAME: The name of the package archive using the Git tag (from 'variables' of 'sphinx-gitlab.yaml').
# - PACKAGE_TAG_REGISTRY_URL: The GitLab API URL used to upload the archives (from 'variables' of 'sphinx-gitlab.yaml').
# ------------------------------------------------------------------------------

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

echo "Uploading HTML documentation package"

curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
     --upload-file "dist/${PACKAGE_SHA_FILE_NAME}" \
     "${PACKAGE_TAG_REGISTRY_URL}/${PACKAGE_TAG_FILE_NAME}"

echo "Releasing HTML documentation"

release-cli create \
  --name "Version ${CI_COMMIT_TAG}" \
  --tag-name "${CI_COMMIT_TAG}" \
  --assets-link "{ \"name\": \"${PACKAGE_TAG_FILE_NAME}\", \"url\": \"${PACKAGE_TAG_REGISTRY_URL}/${PACKAGE_TAG_FILE_NAME}\" }"
