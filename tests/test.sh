#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

# shellcheck source-path=SCRIPTDIR/.. disable=SC1090
source "${PROJECT_DIR}/lib/ansi/ansi"

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    ansi --bold --red " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    ansi --bold --red " ┃ TESTS -- Failed!                                                             "
    ansi --bold --red " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"

    exit 1
}

trap 'error_handler ${?}' ERR

"${PROJECT_DIR}/tests/gitlab/test-gitlab-check-variables.sh"
"${PROJECT_DIR}/tests/gitlab/test-gitlab-extended-variables.sh"

"${PROJECT_DIR}/tests/maven/test-maven-build.sh"
"${PROJECT_DIR}/tests/maven/test-maven-configure.sh"
"${PROJECT_DIR}/tests/maven/test-maven-deploy.sh"
"${PROJECT_DIR}/tests/maven/test-maven-quality.sh"
"${PROJECT_DIR}/tests/maven/test-maven-release.sh"

"${PROJECT_DIR}/tests/sphinx/test-sphinx-build-html.sh"
"${PROJECT_DIR}/tests/sphinx/test-sphinx-check-commit-tag.sh"
"${PROJECT_DIR}/tests/sphinx/test-sphinx-check-image-version.sh"
"${PROJECT_DIR}/tests/sphinx/test-sphinx-release.sh"

ansi --bold --green " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
ansi --bold --green " ┃ TESTS -- Perfect!                                                            "
ansi --bold --green " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
