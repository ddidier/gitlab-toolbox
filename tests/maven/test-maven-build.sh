#!/usr/bin/env bash

PROJECT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../.." && pwd)"

LIB_DIR="${PROJECT_DIR}/lib"
SRC_DIR="${PROJECT_DIR}/src"
TESTS_DIR="${PROJECT_DIR}/tests"

# shellcheck disable=SC2034
NDD_TEST4B_LIB_DIR="${LIB_DIR}"
# shellcheck disable=SC2034
NDD_TEST4B_SCRIPT_NAME="maven-build.sh"
# shellcheck source-path=SCRIPTDIR/../../lib disable=SC1090
source "${PROJECT_DIR}/lib/ndd-test4b/ndd-test4b.sh"

# ------------------------------------------------------------------------------------------------ tests lifecycle -----

main() {
  ndd::test::main
}

setUp() {
  ndd::test::setUp

  export CI_PROJECT_DIR="${test_directory}"
  export SONAR_TOKEN="MY_SONAR_TOKEN"
  export SONAR_URL="MY_SONAR_URL"
}

tearDown() {
  ndd::test::tearDown

  unset CI_PROJECT_DIR
  unset SONAR_TOKEN
  unset SONAR_URL
}

# ------------------------------------------------------------------------------------------------ tests utilities -----

# ---------------------------------------------------------------------------------------------------------- tests -----

test__calls_maven-configure() {
  add_mock_directory "${TESTS_DIR}/maven/mocks/test-maven-build"

  if ! "${SRC_DIR}/maven/maven-build.sh" > "${captured_stdout}"; then
    print_captured_data
    fail "test__calls_maven-configure"
  fi

  assertCapturedStdoutContains "Printing Maven effective settings"
}

test__calls_mvn_verify() {
  add_mock_directory "${TESTS_DIR}/maven/mocks/test-maven-build"

  if ! "${SRC_DIR}/maven/maven-build.sh" > "${captured_stdout}"; then
    print_captured_data
    fail "test__calls_mvn_verify"
  fi

  assertCapturedStdoutContains "Running Maven verify"
}

test__does_not_call_mvn_lock() {
  add_mock_directory "${TESTS_DIR}/maven/mocks/test-maven-build"

  if ! "${SRC_DIR}/maven/maven-build.sh" > "${captured_stdout}"; then
    print_captured_data
    fail "test__does_not_call_mvn_lock"
  fi

  assertCapturedStdoutDoesNotContain "Running Maven lock"
}

test__calls_mvn_lock() {
  add_mock_directory "${TESTS_DIR}/maven/mocks/test-maven-build"

  if ! "${SRC_DIR}/maven/maven-build.sh" --check-dependencies > "${captured_stdout}"; then
    print_captured_data
    fail "test__calls_mvn_lock"
  fi

  assertCapturedStdoutContains "Running Maven lock"
}

test__does_not_convert_coverage() {
  add_mock_directory "${TESTS_DIR}/maven/mocks/test-maven-build"

  if ! "${SRC_DIR}/maven/maven-build.sh" > "${captured_stdout}"; then
    print_captured_data
    fail "test__does_not_convert_coverage"
  fi

  assertCapturedStdoutDoesNotContain "covered"
}

test__converts_coverage() {
  add_mock_directory "${TESTS_DIR}/maven/mocks/test-maven-build"

  mkdir -p "${test_directory}/target/site/jacoco"
  {
    echo "GROUP,PACKAGE,CLASS,INSTRUCTION_MISSED,INSTRUCTION_COVERED,BRANCH_MISSED,BRANCH_COVERED,LINE_MISSED,LINE_COVERED,COMPLEXITY_MISSED,COMPLEXITY_COVERED,METHOD_MISSED,METHOD_COVERED"
    echo "Some Library,some.library,SomeClass1,10,65,0,6,0,14,0,7,0,4"
    echo "Some Library,some.library,SomeClass2,0,8,0,0,0,3,0,2,0,2"
    echo "Some Library,some.library,SomeClass3,0,8,0,0,0,3,0,2,0,2"
    echo "Some Library,some.library,SomeClass4,0,53,0,0,0,12,0,11,0,11"
    echo "Some Library,some.library,SomeClass5,0,28,0,2,0,6,0,3,0,2"
    echo "Some Library,some.library,SomeClass6,0,23,0,2,0,6,0,3,0,2"
    echo "Some Library,some.library,SomeClass7,0,8,0,0,0,3,0,2,0,2"
    echo "Some Library,some.library,SomeClass8,0,59,0,6,0,14,0,7,0,4"
    echo "Some Library,some.library,SomeClass9,0,101,0,8,0,22,0,12,0,8"
  } > "${test_directory}/target/site/jacoco/jacoco.csv"

  if ! "${SRC_DIR}/maven/maven-build.sh" > "${captured_stdout}"; then
    print_captured_data
    fail "test__convert_coverage"
  fi

  assertCapturedStdoutContains "353 / 363 instructions covered"
  assertCapturedStdoutContains "97.25% covered"
}

# ----------------------------------------------------------------------------------------------------------------------

main "${@}"
