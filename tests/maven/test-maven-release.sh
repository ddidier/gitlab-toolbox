#!/usr/bin/env bash

PROJECT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../.." && pwd)"

LIB_DIR="${PROJECT_DIR}/lib"
SRC_DIR="${PROJECT_DIR}/src"
TESTS_DIR="${PROJECT_DIR}/tests"

# shellcheck disable=SC2034
NDD_TEST4B_LIB_DIR="${LIB_DIR}"
# shellcheck disable=SC2034
NDD_TEST4B_SCRIPT_NAME="maven-release.sh"
# shellcheck source-path=SCRIPTDIR/../../lib disable=SC1090
source "${PROJECT_DIR}/lib/ndd-test4b/ndd-test4b.sh"

# ------------------------------------------------------------------------------------------------ tests lifecycle -----

main() {
  ndd::test::main
}

setUp() {
  ndd::test::setUp
}

tearDown() {
  ndd::test::tearDown

  unset CI_COMMIT_TAG
}

# ------------------------------------------------------------------------------------------------ tests utilities -----

# ---------------------------------------------------------------------------------------------------------- tests -----

test__a_not_very_interesting_case() {
  add_mock_directory "${TESTS_DIR}/maven/mocks/test-maven-release"

  export CI_COMMIT_TAG="1.0.0"

  if ! "${SRC_DIR}/maven/maven-release.sh" > "${captured_stdout}"; then
    print_captured_data
    fail "test__a_not_very_interesting_case"
  fi

  assertCapturedStdoutContains "Creating release"
}

# ----------------------------------------------------------------------------------------------------------------------

main "${@}"
