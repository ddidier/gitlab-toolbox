#!/usr/bin/env bash

PROJECT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../.." && pwd)"

LIB_DIR="${PROJECT_DIR}/lib"
SRC_DIR="${PROJECT_DIR}/src"
TESTS_DIR="${PROJECT_DIR}/tests"

# shellcheck disable=SC2034
NDD_TEST4B_LIB_DIR="${LIB_DIR}"
# shellcheck disable=SC2034
NDD_TEST4B_SCRIPT_NAME="maven-configure.sh"
# shellcheck source-path=SCRIPTDIR/../../lib disable=SC1090
source "${PROJECT_DIR}/lib/ndd-test4b/ndd-test4b.sh"

# ------------------------------------------------------------------------------------------------ tests lifecycle -----

main() {
  ndd::test::main
}

setUp() {
  ndd::test::setUp

  export CI_PROJECT_DIR="${test_directory}"
  export SONAR_TOKEN="MY_SONAR_TOKEN"
  export SONAR_URL="MY_SONAR_URL"
}

tearDown() {
  ndd::test::tearDown

  unset CI_PROJECT_DIR
  unset SONAR_TOKEN
  unset SONAR_URL
}

# ------------------------------------------------------------------------------------------------ tests utilities -----

# ---------------------------------------------------------------------------------------------------------- tests -----

test__calls_mvn() {
  add_mock_directory "${TESTS_DIR}/maven/mocks/test-maven-configure"


  if ! "${SRC_DIR}/maven/maven-configure.sh" > "${captured_stdout}"; then
    print_captured_data
    fail "test__calls_mvn"
  fi

  assertDirectoryExists "${test_directory}/.m2"
  assertCapturedStdoutContains "Printing Maven effective settings"
}

# ----------------------------------------------------------------------------------------------------------------------

main "${@}"
