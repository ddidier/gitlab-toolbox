#!/usr/bin/env bash

PROJECT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../.." && pwd)"

LIB_DIR="${PROJECT_DIR}/lib"
SRC_DIR="${PROJECT_DIR}/src"

# shellcheck disable=SC2034
NDD_TEST4B_LIB_DIR="${LIB_DIR}"
# shellcheck disable=SC2034
NDD_TEST4B_SCRIPT_NAME="sphinx-check-commit-tag.sh"
# shellcheck source-path=SCRIPTDIR/../../lib disable=SC1090
source "${PROJECT_DIR}/lib/ndd-test4b/ndd-test4b.sh"

# ------------------------------------------------------------------------------------------------ tests lifecycle -----

main() {
  ndd::test::main
}

setUp() {
  ndd::test::setUp

  unset CI_COMMIT_TAG
}

tearDown() {
  ndd::test::tearDown
}

# ------------------------------------------------------------------------------------------------ tests utilities -----

# ---------------------------------------------------------------------------------------------------------- tests -----

test__succeeds_when_no_tag_exists() {
  if ! "${SRC_DIR}/sphinx/sphinx-check-commit-tag.sh" > "${captured_stdout}"; then
    print_captured_data
    fail "test__succeeds_when_no_tag_exists"
  fi

  assertCapturedStdoutContains "Git commit tag is undefined"
}

test__succeeds_when_a_valid_tag_exists() {
  export CI_COMMIT_TAG="1.2.3"

  if ! "${SRC_DIR}/sphinx/sphinx-check-commit-tag.sh" > "${captured_stdout}"; then
    print_captured_data
    fail "test__succeeds_when_a_valid_tag_exists"
  fi

  assertCapturedStdoutContains "Git commit tag is defined as '1.2.3'"
  assertCapturedStdoutContains "Git commit tag is valid"
}

test__succeeds_when_an_invalid_tag_exists() {
  for tag in "" "1" "1.2" "1.2.3.4" "a" "a.b" "a.b.c" "1.a" "1.2.a" "1.a.b" "a.1" "a.1.2"; do
    export CI_COMMIT_TAG="${tag}"

    if "${SRC_DIR}/sphinx/sphinx-check-commit-tag.sh" > "${captured_stdout}"; then
      print_captured_data
      fail "test__succeeds_when_an_invalid_tag_exists"
    fi

    assertCapturedStdoutContains "Git commit tag is defined as '${tag}'"
    assertCapturedStdoutContains "Git commit tag does not conform to 'X.Y.Z'"
  done
}

# ----------------------------------------------------------------------------------------------------------------------

main "${@}"
