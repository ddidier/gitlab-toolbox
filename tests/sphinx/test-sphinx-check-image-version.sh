#!/usr/bin/env bash

PROJECT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../.." && pwd)"

LIB_DIR="${PROJECT_DIR}/lib"
SRC_DIR="${PROJECT_DIR}/src"

# shellcheck disable=SC2034
NDD_TEST4B_LIB_DIR="${LIB_DIR}"
# shellcheck disable=SC2034
NDD_TEST4B_SCRIPT_NAME="sphinx-check-image-version.sh"
# shellcheck source-path=SCRIPTDIR/../../lib disable=SC1090
source "${PROJECT_DIR}/lib/ndd-test4b/ndd-test4b.sh"

# ------------------------------------------------------------------------------------------------ tests lifecycle -----

main() {
  ndd::test::main
}

setUp() {
  ndd::test::setUp

  unset DDIDIER_SPHINX_IMAGE_VERSION
  unset GIT_COMMIT_TAG
}

tearDown() {
  ndd::test::tearDown
}

# ------------------------------------------------------------------------------------------------ tests utilities -----

# ---------------------------------------------------------------------------------------------------------- tests -----

test__fails_when_DDIDIER_SPHINX_IMAGE_VERSION_is_not_set() {
  export GIT_COMMIT_TAG="1.2.3"

  if "${SRC_DIR}/sphinx/sphinx-check-image-version.sh" > "${captured_stdout}"; then
    print_captured_data
    fail "test__fails_when_DDIDIER_SPHINX_IMAGE_VERSION_is_not_set"
  fi

  assertCapturedStdoutContains "The environment variable 'DDIDIER_SPHINX_IMAGE_VERSION' is not set."
}

test__fails_when_GIT_COMMIT_TAG_is_not_set() {
  export DDIDIER_SPHINX_IMAGE_VERSION="1.2.3"

  if "${SRC_DIR}/sphinx/sphinx-check-image-version.sh" > "${captured_stdout}"; then
    print_captured_data
    fail "test__fails_when_GIT_COMMIT_TAG_is_not_set"
  fi

  assertCapturedStdoutContains "The environment variable 'GIT_COMMIT_TAG' is not set."
}

test__fails_when_DDIDIER_SPHINX_IMAGE_VERSION_and_GIT_COMMIT_TAG_are_different() {
  export DDIDIER_SPHINX_IMAGE_VERSION="1.2.3"
  export GIT_COMMIT_TAG="4.5.6"

  if "${SRC_DIR}/sphinx/sphinx-check-image-version.sh" > "${captured_stdout}"; then
    print_captured_data
    fail "test__fails_when_DDIDIER_SPHINX_IMAGE_VERSION_and_GIT_COMMIT_TAG_are_different"
  fi

  assertCapturedStdoutContains "Actual Docker image version   = 4.5.6"
  assertCapturedStdoutContains "Expected Docker image version = 1.2.3"
  assertCapturedStdoutContains "Docker image versions are different. Aborting!"
}

test__succeeds_when_DDIDIER_SPHINX_IMAGE_VERSION_and_GIT_COMMIT_TAG_are_equal() {
  export DDIDIER_SPHINX_IMAGE_VERSION="1.2.3"
  export GIT_COMMIT_TAG="1.2.3"

  if ! "${SRC_DIR}/sphinx/sphinx-check-image-version.sh" > "${captured_stdout}"; then
    print_captured_data
    fail "test__succeeds_when_DDIDIER_SPHINX_IMAGE_VERSION_and_GIT_COMMIT_TAG_are_equal"
  fi

  assertCapturedStdoutContains "Docker image version is valid"
}

# ----------------------------------------------------------------------------------------------------------------------

main "${@}"
