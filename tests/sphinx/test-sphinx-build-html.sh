#!/usr/bin/env bash

PROJECT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../.." && pwd)"

LIB_DIR="${PROJECT_DIR}/lib"
SRC_DIR="${PROJECT_DIR}/src"
TESTS_DIR="${PROJECT_DIR}/tests"

# shellcheck disable=SC2034
NDD_TEST4B_LIB_DIR="${LIB_DIR}"
# shellcheck disable=SC2034
NDD_TEST4B_SCRIPT_NAME="sphinx-build-html.sh"
# shellcheck source-path=SCRIPTDIR/../../lib disable=SC1090
source "${PROJECT_DIR}/lib/ndd-test4b/ndd-test4b.sh"

# ------------------------------------------------------------------------------------------------ tests lifecycle -----

main() {
  ndd::test::main
}

setUp() {
  ndd::test::setUp
}

tearDown() {
  ndd::test::tearDown
}

# ------------------------------------------------------------------------------------------------ tests utilities -----

# ---------------------------------------------------------------------------------------------------------- tests -----

test__calls_make() {
  add_mock_directory "${TESTS_DIR}/sphinx/mocks/test-sphinx-build-html"

  if ! "${SRC_DIR}/sphinx/sphinx-build-html.sh" > "${captured_stdout}"; then
    print_captured_data
    fail "test__calls_make"
  fi

  assertCapturedStdoutContains "Serving the Sphinx documentation"
}

# ----------------------------------------------------------------------------------------------------------------------

main "${@}"
