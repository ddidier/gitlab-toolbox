#!/usr/bin/env bash

PROJECT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../.." && pwd)"

LIB_DIR="${PROJECT_DIR}/lib"
SRC_DIR="${PROJECT_DIR}/src"
TESTS_DIR="${PROJECT_DIR}/tests"

# shellcheck disable=SC2034
NDD_TEST4B_LIB_DIR="${LIB_DIR}"
# shellcheck disable=SC2034
NDD_TEST4B_SCRIPT_NAME="sphinx-release.sh"
# shellcheck source-path=SCRIPTDIR/../../lib disable=SC1090
source "${PROJECT_DIR}/lib/ndd-test4b/ndd-test4b.sh"

# ------------------------------------------------------------------------------------------------ tests lifecycle -----

main() {
  ndd::test::main
}

setUp() {
  ndd::test::setUp

  unset CI_COMMIT_TAG
  unset CI_JOB_TOKEN
  unset PACKAGE_SHA_FILE_NAME
  unset PACKAGE_TAG_FILE_NAME
  unset PACKAGE_TAG_REGISTRY_URL
}

tearDown() {
  ndd::test::tearDown
}

# ------------------------------------------------------------------------------------------------ tests utilities -----

# ---------------------------------------------------------------------------------------------------------- tests -----

test__a_not_very_interesting_case() {
  add_mock_directory "${TESTS_DIR}/sphinx/mocks/test-sphinx-release"

  export CI_COMMIT_TAG="1.0.0"
  export CI_JOB_TOKEN="some-job-token"
  export PACKAGE_SHA_FILE_NAME="my-project-0123456789abcdef.tar.gz"
  export PACKAGE_TAG_FILE_NAME="my-project-1.0.0.tar.gz"
  export PACKAGE_TAG_REGISTRY_URL="https://package/registry/url"

  if ! "${SRC_DIR}/sphinx/sphinx-release.sh" > "${captured_stdout}"; then
    print_captured_data
    fail "test__a_not_very_interesting_case"
  fi

  assertCapturedStdoutContains "Uploading archive"
  assertCapturedStdoutContains "Creating release"
}

# ----------------------------------------------------------------------------------------------------------------------

main "${@}"
