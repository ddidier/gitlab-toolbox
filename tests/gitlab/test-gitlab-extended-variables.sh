#!/usr/bin/env bash

PROJECT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../.." && pwd)"

LIB_DIR="${PROJECT_DIR}/lib"
SRC_DIR="${PROJECT_DIR}/src"

# shellcheck disable=SC2034
NDD_TEST4B_LIB_DIR="${LIB_DIR}"
# shellcheck disable=SC2034
NDD_TEST4B_SCRIPT_NAME="gitlab-extended-variables.sh"
# shellcheck source-path=SCRIPTDIR/../../lib disable=SC1090
source "${PROJECT_DIR}/lib/ndd-test4b/ndd-test4b.sh"

# ------------------------------------------------------------------------------------------------ tests lifecycle -----

main() {
  ndd::test::main
}

setUp() {
  ndd::test::setUp

  export CI_JOB_ID=""
  export CI_PAGES_DOMAIN=""
  export CI_SERVER_PROTOCOL=""
  export CI_PROJECT_PATH=""
  export CI_PROJECT_ROOT_NAMESPACE=""

#  unset CI_JOB_ID
#  unset CI_PAGES_DOMAIN
#  unset CI_PROJECT_PATH
#  unset CI_PROJECT_ROOT_NAMESPACE
#  unset CI_SERVER_PROTOCOL
}

tearDown() {
  ndd::test::tearDown
}

# ------------------------------------------------------------------------------------------------ tests utilities -----

# ---------------------------------------------------------------------------------------------------------- tests -----

# ---------- CI_PROJECT_CHILD_NAMESPACE

test__CI_PROJECT_CHILD_NAMESPACE_with_no_subgroup() {
  export CI_PROJECT_PATH="gitlab-org/gitlab-foss"
  export CI_PROJECT_ROOT_NAMESPACE="gitlab-org"

  # shellcheck source-path=SCRIPTDIR/../../src disable=SC1090
  source "${SRC_DIR}/gitlab/gitlab-extended-variables.sh"

  assertEquals "gitlab-foss" "${CI_PROJECT_CHILD_NAMESPACE}"
}

test__CI_PROJECT_CHILD_NAMESPACE_with_one_subgroup() {
  export CI_PROJECT_PATH="gitlab-org/gitlab-foss/subgroup"
  export CI_PROJECT_ROOT_NAMESPACE="gitlab-org"

  # shellcheck source-path=SCRIPTDIR/../../src disable=SC1090
  source "${SRC_DIR}/gitlab/gitlab-extended-variables.sh"

  assertEquals "gitlab-foss/subgroup" "${CI_PROJECT_CHILD_NAMESPACE}"
}

test__CI_PROJECT_CHILD_NAMESPACE_with_two_subgroups() {
  export CI_PROJECT_PATH="gitlab-org/gitlab-foss/subgroup1/subgroup2"
  export CI_PROJECT_ROOT_NAMESPACE="gitlab-org"

  # shellcheck source-path=SCRIPTDIR/../../src disable=SC1090
  source "${SRC_DIR}/gitlab/gitlab-extended-variables.sh"

  assertEquals "gitlab-foss/subgroup1/subgroup2" "${CI_PROJECT_CHILD_NAMESPACE}"
}

# ---------- CI_JOB_ARTIFACTS_URL

test__CI_JOB_ARTIFACTS_URL_with_no_subgroup() {
  export CI_JOB_ID="12345"
  export CI_PAGES_DOMAIN="gitlab.io"
  export CI_SERVER_PROTOCOL="https"
  export CI_PROJECT_PATH="gitlab-org/gitlab-foss"
  export CI_PROJECT_ROOT_NAMESPACE="gitlab-org"

  # shellcheck source-path=SCRIPTDIR/../../src disable=SC1090
  source "${SRC_DIR}/gitlab/gitlab-extended-variables.sh"

  assertEquals "https://gitlab-org.gitlab.io/-/gitlab-foss/-/jobs/12345/artifacts" "${CI_JOB_ARTIFACTS_URL}"
}

test__CI_JOB_ARTIFACTS_URL_with_one_subgroup() {
  export CI_JOB_ID="12345"
  export CI_PAGES_DOMAIN="gitlab.io"
  export CI_SERVER_PROTOCOL="https"
  export CI_PROJECT_PATH="gitlab-org/gitlab-foss/subgroup"
  export CI_PROJECT_ROOT_NAMESPACE="gitlab-org"

  # shellcheck source-path=SCRIPTDIR/../../src disable=SC1090
  source "${SRC_DIR}/gitlab/gitlab-extended-variables.sh"

  assertEquals "https://gitlab-org.gitlab.io/-/gitlab-foss/subgroup/-/jobs/12345/artifacts" "${CI_JOB_ARTIFACTS_URL}"
}

test__CI_JOB_ARTIFACTS_URL_with_two_subgroups() {
  export CI_JOB_ID="12345"
  export CI_PAGES_DOMAIN="gitlab.io"
  export CI_SERVER_PROTOCOL="https"
  export CI_PROJECT_PATH="gitlab-org/gitlab-foss/subgroup1/subgroup2"
  export CI_PROJECT_ROOT_NAMESPACE="gitlab-org"

  # shellcheck source-path=SCRIPTDIR/../../src disable=SC1090
  source "${SRC_DIR}/gitlab/gitlab-extended-variables.sh"

  assertEquals "https://gitlab-org.gitlab.io/-/gitlab-foss/subgroup1/subgroup2/-/jobs/12345/artifacts" "${CI_JOB_ARTIFACTS_URL}"
}

# ----------------------------------------------------------------------------------------------------------------------

main "${@}"
