#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

# shellcheck source-path=SCRIPTDIR/.. disable=SC1090
source "${PROJECT_DIR}/lib/ansi/ansi"



# ---------- BashCov is a Ruby program:

if ! command -v bundle > /dev/null; then
  ansi --bold --red " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  ansi --bold --red " ┃ This setup script requires 'bundler' to be installed!                        "
  ansi --bold --red " ┃ Use for example RVM (https://rvm.io/rubies/).                                "
  ansi --bold --red " ┃ Aborting...                                                                  "
  ansi --bold --red " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  exit 1
fi

# rvm install "$(cat "${PROJECT_DIR}/.ruby-version")"

# rvm gemset create "$(cat "${PROJECT_DIR}/.ruby-gemset")"

# rvm gemset use "$(cat "${PROJECT_DIR}/.ruby-version")@$(cat "${PROJECT_DIR}/.ruby-gemset")"

bundle install



# ---------- The ShellCheck Docker image is really slow:

SHELLCHECK_VERSION=0.8.0

curl -L "https://github.com/koalaman/shellcheck/releases/download/v${SHELLCHECK_VERSION}/shellcheck-v${SHELLCHECK_VERSION}.linux.x86_64.tar.xz" \
  --output "${PROJECT_DIR}/lib/shellcheck-v${SHELLCHECK_VERSION}.linux.x86_64.tar.xz"

tar -xf "${PROJECT_DIR}/lib/shellcheck-v${SHELLCHECK_VERSION}.linux.x86_64.tar.xz" -C "${PROJECT_DIR}/lib/"

rm -f "${PROJECT_DIR}/lib/shellcheck"

ln -sf "shellcheck-v${SHELLCHECK_VERSION}" "${PROJECT_DIR}/lib/shellcheck"

rm -f "${PROJECT_DIR}/lib/shellcheck-v${SHELLCHECK_VERSION}.linux.x86_64.tar.xz"



# ----------

ansi --bold --green " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
ansi --bold --green " ┃ ENVIRONMENT SETUP -- Done!                                                   "
ansi --bold --green " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
